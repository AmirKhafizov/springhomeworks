package ru.iteco.controller;

import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.iteco.json.dto.PlaceDto;
import ru.iteco.services.PlaceService;

import java.net.URI;
import java.util.UUID;

/**
 * Контроллер заведений
 */
@RestController
@RequestMapping("/api/v1/places")
@Api(tags = "Place Controller")
public class PlaceController {

    private PlaceService placeService;

    public PlaceController(PlaceService placeService) {
        this.placeService = placeService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<PlaceDto> getPlace(@PathVariable("id") UUID placeId){
        return ResponseEntity.ok(placeService.getPlaceDto(placeId));
    }

    @PostMapping
    public ResponseEntity<PlaceDto> createPlace(@RequestBody PlaceDto dto,
                                                UriComponentsBuilder uriComponentsBuilder){
        PlaceDto placeDto = placeService.createPlace(dto);
        URI uri = uriComponentsBuilder.path("/api/v1/places" + placeDto.getId()).buildAndExpand(placeDto).toUri();
        return ResponseEntity.created(uri).body(placeDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<PlaceDto> updatePlace(@RequestBody PlaceDto dto,
                                @PathVariable("id") UUID placeId){
        PlaceDto placeDto = placeService.updatePlace(placeId, dto);
        if(!placeId.equals(placeDto.getId())){
            ResponseEntity.badRequest().body(placeDto);
        }
        return ResponseEntity.ok(placeDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePlace(@PathVariable("id") UUID placeId){
        placeService.deletePlace(placeId);
        return ResponseEntity.ok(HttpStatus.OK.toString());
    }
}
