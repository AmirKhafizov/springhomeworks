package ru.iteco.controller;

import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.iteco.json.dto.DishCategoryDto;
import ru.iteco.services.DishCategoryService;

import java.net.URI;
import java.util.UUID;

/**
 * Контроллер категорий блюд
 */
@RestController
@RequestMapping("/api/v1/categories")
@Api(tags = "Dish Category Controller")
public class DishCategoryController {

    private DishCategoryService dishCategoryService;

    public DishCategoryController(DishCategoryService dishCategoryService) {
        this.dishCategoryService = dishCategoryService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<DishCategoryDto> getDishCategory(@PathVariable(value = "id") UUID dishCategoryId) {
        return ResponseEntity.ok(dishCategoryService.getDishCategoryDto(dishCategoryId));
    }

    @PostMapping
    public ResponseEntity<DishCategoryDto> createDishCategory(@RequestBody DishCategoryDto dto,
                                                             UriComponentsBuilder uriComponentsBuilder){
        DishCategoryDto dishCategoryDto = dishCategoryService.createDishCategory(dto);
        URI uri = uriComponentsBuilder.path("/api/v1/categories" + dishCategoryDto.getId())
                .buildAndExpand(dishCategoryDto).toUri();
        return ResponseEntity.created(uri).body(dishCategoryDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<DishCategoryDto> updateDishCategory(@RequestBody DishCategoryDto dto,
                                      @PathVariable("id") UUID dishCategoryId){
        DishCategoryDto dishCategoryDto = dishCategoryService.updateDishCategory(dishCategoryId, dto);
        if(!dishCategoryId.equals(dishCategoryDto.getId())){
            return ResponseEntity.badRequest().body(dishCategoryDto);
        }
        return ResponseEntity.ok(dishCategoryDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteDishCategory(@PathVariable("id") UUID dishCategoryId){
        dishCategoryService.deleteDishCategory(dishCategoryId);
        return ResponseEntity.ok(HttpStatus.OK.toString());
    }


}
