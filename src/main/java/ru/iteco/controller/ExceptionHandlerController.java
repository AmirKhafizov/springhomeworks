package ru.iteco.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.iteco.exception.BadRequestException;
import ru.iteco.exception.NotFoundException;
import ru.iteco.json.dto.ExceptionDto;

/**
 * Контроллер отлова ошибок
 */
@RestControllerAdvice(basePackages = "ru.iteco.controller")
@ConfigurationProperties(prefix = "spring.application.name")
public class ExceptionHandlerController {

    private String system;

    @ExceptionHandler({BadRequestException.class, IllegalArgumentException.class})
    public ResponseEntity<ExceptionDto> handleBadRequest(BadRequestException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ExceptionDto.of(ex.getMessage(),system));
    }

    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<ExceptionDto> handleNotFound(NotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ExceptionDto.of(ex.getMessage(),system));
    }
}
