package ru.iteco.controller;

import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.iteco.json.dto.DishDto;
import ru.iteco.services.DishService;

import java.net.URI;
import java.util.UUID;

/**
 * Контроллер блюд
 */
@RestController
@RequestMapping("/api/v1/dishes")
@Api(tags = "Dish Controller")
public class DishController {

    private DishService dishService;

    public DishController(DishService dishService) {
        this.dishService = dishService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<DishDto> getDish(@PathVariable("id") UUID dishId){
        return ResponseEntity.ok(dishService.getDishDto(dishId));
    }

    @PostMapping
    public ResponseEntity<DishDto> createDish(@RequestBody DishDto dto,
                              UriComponentsBuilder uriComponentsBuilder){
        DishDto dishDto = dishService.createDish(dto);
        URI uri = uriComponentsBuilder.path("/api/v1/dishes" + dishDto.getId()).buildAndExpand(dishDto).toUri();
        return ResponseEntity.created(uri).body(dishDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<DishDto> updateDish(@RequestBody DishDto dto,
                              @PathVariable("id") UUID dishId){
        DishDto dishDto = dishService.updateDish(dishId, dto);
        if(!dishId.equals(dishDto.getId())){
           return ResponseEntity.badRequest().body(dishDto);
        }
        return ResponseEntity.ok(dishDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteDish(@PathVariable("id") UUID dishId){
        dishService.deleteDish(dishId);
        return ResponseEntity.ok(HttpStatus.OK.toString());
    }
}
