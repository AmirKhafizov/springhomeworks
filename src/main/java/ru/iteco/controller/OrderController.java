package ru.iteco.controller;

import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.iteco.json.dto.OrderDto;
import ru.iteco.services.OrderService;

import java.net.URI;
import java.util.UUID;

/**
 * Контроллер заказов доставки
 */
@RestController
@RequestMapping("/api/v1/orders")
@Api(tags = "Order Controller")
public class OrderController {

    private OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderDto> getOrder(@PathVariable("id") UUID orderId) {
        return ResponseEntity.ok(orderService.getOrderDto(orderId));
    }

    @PostMapping
    public ResponseEntity<OrderDto> createOrder(@RequestBody OrderDto dto,
                                                UriComponentsBuilder uriComponentsBuilder) {
        OrderDto orderDto = orderService.createOrder(dto);
        URI uri = uriComponentsBuilder.path("/api/v1/orders" + orderDto.getId()).buildAndExpand(orderDto).toUri();
        return ResponseEntity.created(uri).body(orderDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<OrderDto> updateOrder(@RequestBody OrderDto dto,
                                @PathVariable("id") UUID orderId) {
        OrderDto orderDto = orderService.updateOrder(orderId, dto);
        if(!orderId.equals(orderDto.getId())){
            return ResponseEntity.badRequest().body(orderDto);
        }
        return ResponseEntity.ok(orderDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteOrder(@PathVariable("id") UUID orderId) {
        orderService.deleteOrder(orderId);
        return ResponseEntity.ok(HttpStatus.OK.toString());
    }
}
