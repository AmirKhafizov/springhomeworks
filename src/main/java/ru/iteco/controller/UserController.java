package ru.iteco.controller;

import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import ru.iteco.json.dto.UserDto;
import ru.iteco.services.UserService;

import java.net.URI;
import java.util.UUID;

/**
 * Контроллер пользователей
 */
@RestController
@RequestMapping("/api/v1/users")
@Api(tags = "User Controller")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<UserDto> register(@RequestBody UserDto dto,
                                            UriComponentsBuilder uriComponentsBuilder){
        UserDto newUser = userService.register(dto);
        URI uri = uriComponentsBuilder.path("/api/v1/users" + newUser.getId()).buildAndExpand(newUser).toUri();
        return ResponseEntity.created(uri).body(newUser);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable("id")UUID uuid){
        return ResponseEntity.ok(userService.getUserDto(uuid));
    }
}
