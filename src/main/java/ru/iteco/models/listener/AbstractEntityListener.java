package ru.iteco.models.listener;

import ru.iteco.models.AbstractEntity;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class AbstractEntityListener {

    @PrePersist
    private void setCreateAt(final AbstractEntity abstractEntity) {
        LocalDateTime now = LocalDateTime.now();
        ZonedDateTime nowWithZone = ZonedDateTime.of(now, ZoneId.of("UTC"));
        abstractEntity.setCreatedAt(Instant.from(nowWithZone));
        abstractEntity.setUpdatedAt(abstractEntity.getCreatedAt());
    }

    @PreUpdate
    private void setUpdateAt(final AbstractEntity abstractEntity) {
        LocalDateTime now = LocalDateTime.now();
        ZonedDateTime nowWithZone = ZonedDateTime.of(now, ZoneId.of("UTC"));
        abstractEntity.setUpdatedAt(Instant.from(nowWithZone));
    }

}

