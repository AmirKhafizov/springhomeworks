package ru.iteco.models;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import java.time.Instant;
import java.time.LocalTime;
import java.util.UUID;

/**
 * Класс заведения, осуществдяющего доставку
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Place extends AbstractEntity {
    /**
     * Поле название заведения
     */
    @Column(nullable = false)
    private String name;

    /**
     * Поле краткое описание заведения
     */
    @Column(nullable = false)
    private String about;

    /**
     * Поле адресс заведения
     */
    @Column(nullable = false)
    private String address;

    /**
     * Поле телефон заведения
     */
    @Column(nullable = false,unique = true)
    private String phone;

    /**
     * Поле администратор заведения
     */
    @OneToOne(fetch = FetchType.LAZY)
    private User user;

    /**
     * Поле время на доставку
     */
    @Column(nullable = false)
    private LocalTime timeOfDelivery;
}
