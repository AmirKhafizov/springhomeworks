package ru.iteco.models.menu;

import lombok.*;
import ru.iteco.models.AbstractEntity;
import ru.iteco.models.Place;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.UUID;


/**
 * Класс блюд
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Dish extends AbstractEntity {
    /**
     * Поле название блюда
     */
    @Column(nullable = false)
    private String name;

    /**
     * Поле описание блюда
     */
    @Column(nullable = false)
    private String description;

    /**
     * Поле коли-во калорий блюда
     */
    @Column(nullable = false)
    private Integer calorieCount;

    /**
     * Поле заведения
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "place_id")
    private Place place;

    /**
     * Поле категории блюда
     */
    @ManyToOne(fetch = FetchType.LAZY)
    private DishCategory dishCategory;

    /**
     * Поле цены блюда
     */
    @Column(nullable = false)
    private BigDecimal price;

    /**
     * Поле граммовка блюда
     */
    private BigInteger weight;
}
