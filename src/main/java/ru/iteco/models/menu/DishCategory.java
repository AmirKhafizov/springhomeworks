package ru.iteco.models.menu;

import lombok.*;
import ru.iteco.models.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Класс категорий блюд
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class DishCategory extends AbstractEntity {
    /**
     * Поле название категории блюда
     */
    @Column(nullable = false, unique = true)
    private String name;

    /**
     * Поле возможность удаления категории блюда
     */
    @Column(nullable = false)
    private Boolean canRemove;
}
