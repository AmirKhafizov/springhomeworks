package ru.iteco.models;

import lombok.*;
import org.springframework.data.annotation.Id;
import ru.iteco.security.role.Role;

import javax.persistence.*;
import java.util.UUID;


/**
 * Класс пользователей
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User extends AbstractEntity {
    /**
     * Поле почта пользователя
     */
    @Column(nullable = false, unique = true)
    private String email;

    /**
     * Поле телефон пользлователя
     */
    @Column(nullable = false, unique = true)
    private String phone;

    /**
     * Поле хешированный пароль пользователя
     */
    @Column(nullable = false)
    private String password;

    /**
     * Поле роль пользователя
     */
    @Enumerated(EnumType.STRING)
    private Role role;
}
