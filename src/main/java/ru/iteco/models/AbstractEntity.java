package ru.iteco.models;

import lombok.Getter;
import lombok.Setter;
import ru.iteco.models.listener.AbstractEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

/**
 * Абстрактный класс сущностей
 */
@Getter
@Setter
@MappedSuperclass
@EntityListeners(AbstractEntityListener.class)
public abstract class AbstractEntity implements Serializable {

    /**
     * Поле уникального идентификатора
     */
    @Id
    @GeneratedValue
    private UUID id;

    @Column(nullable = false, updatable = false)
    private Instant createdAt;

    @Column(nullable = false)
    private Instant updatedAt;

}
