package ru.iteco.models;

import lombok.*;
import ru.iteco.models.menu.Dish;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

/**
 * Класс заказов
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "orders")
public class Order extends  AbstractEntity {
    /**
     * Поле время начала заказа
     */
    @Column(nullable = false)
    private Instant startTime;

    /**
     * Поле время доставки заказа
     */
    @Column(nullable = false)
    private Instant endTime;

    /**
     * Поле клиента, сделавшего заказ
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    /**
     * Поле блюд, заказанных пользователем
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(joinColumns = @JoinColumn(name = "order_id"), inverseJoinColumns = @JoinColumn(name = "dishe_id"))
    private List<Dish> dishes;

    /**
     * Поле стоимости
     */
    @Column(nullable = false)
    private BigDecimal cost;

    /**
     * Поле заведение, которому отправлен заказ
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "place_id")
    private Place place;
}
