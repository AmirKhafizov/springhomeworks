package ru.iteco.models.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserStatus {
    CREATED("Создан"),
    LOCKED("Заблокирован"),
    ACTIVED("Активен");

    private String description;

}
