package ru.iteco.models.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderStatus {
    ACCEPTED("Принят"),
    CANCELLED("Отменен"),
    PREPARED("Готовится"),
    DELIVERED("Доставлен");

    private String description;
}
