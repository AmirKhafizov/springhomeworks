package ru.iteco.security.role;

public enum Role {
    USER, PLACE_ADMIN, PLACE_DIRECTOR, SUPER_ADMIN
}
