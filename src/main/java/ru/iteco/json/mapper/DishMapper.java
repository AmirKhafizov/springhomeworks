package ru.iteco.json.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.json.dto.DishDto;
import ru.iteco.models.menu.Dish;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Маппер модели блюда в DTO
 */
@Component
public class DishMapper implements Mapper<Dish, DishDto> {

    @Override
    public DishDto map(Dish dish) {
        if (dish == null) return null;
        return DishDto.builder()
                .id(dish.getId())
                .name(dish.getName())
                .description(dish.getDescription())
                .price(dish.getPrice())
                .calorieCount(dish.getCalorieCount())
                .weight(dish.getWeight())
                .dishCategoryId(dish.getDishCategory().getId())
                .placeDtoId(dish.getPlace().getId())
                .build();
    }

    @Override
    public List<DishDto> map(List<Dish> dishes) {
        if (dishes == null){
            return null;
        }
        return dishes.stream().map(this::map).collect(Collectors.toList());
    }
}
