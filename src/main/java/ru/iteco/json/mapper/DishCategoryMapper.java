package ru.iteco.json.mapper;

import org.springframework.stereotype.Component;
import ru.iteco.json.dto.DishCategoryDto;
import ru.iteco.json.dto.DishDto;
import ru.iteco.models.menu.Dish;
import ru.iteco.models.menu.DishCategory;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Маппер модели категории блюда в DTO
 */
@Component
public class DishCategoryMapper implements Mapper<DishCategory, DishCategoryDto> {
    @Override
    public DishCategoryDto map(DishCategory dishCategory) {
        if(dishCategory == null) return null;
        return DishCategoryDto.builder()
                .id(dishCategory.getId())
                .name(dishCategory.getName())
                .canRemove(dishCategory.getCanRemove())
                .build();
    }

    @Override
    public List<DishCategoryDto> map(List<DishCategory> dishCategories) {
        if (dishCategories == null){
            return null;
        }
        return dishCategories.stream().map(this::map).collect(Collectors.toList());
    }
}
