package ru.iteco.json.mapper;

import org.springframework.stereotype.Component;
import ru.iteco.json.dto.PlaceDto;
import ru.iteco.models.Place;

/**
 * Маппер модели заведения в DTO
 */
@Component
public class PlaceMapper implements Mapper<Place, PlaceDto> {

    @Override
    public PlaceDto map(Place place) {
        if (place == null) return null;
        return PlaceDto.builder()
                .id(place.getId())
                .name(place.getName())
                .about(place.getAbout())
                .address(place.getAddress())
                .phone(place.getPhone())
                .timeOfDelivery(place.getTimeOfDelivery())
                .userDtoId(place.getUser().getId())
                .build();
    }
}
