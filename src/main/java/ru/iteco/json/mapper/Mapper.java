package ru.iteco.json.mapper;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Интерфейс конвертер модель в DTO
 *
 * @implNote F - model
 * @implNote T - dto
 */
public interface Mapper<F,T> {
    T map(F f);

    default List<T> map(List<F> f) {
        return f.stream().map(this::map).collect(Collectors.toList());
    }
}
