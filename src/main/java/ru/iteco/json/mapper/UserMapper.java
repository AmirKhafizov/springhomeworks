package ru.iteco.json.mapper;

import org.springframework.stereotype.Component;
import ru.iteco.json.dto.UserDto;
import ru.iteco.models.User;

/**
 * Маппер модели пользователя в DTO
 */
@Component
public class UserMapper implements Mapper<User, UserDto> {
    @Override
    public UserDto map(User user) {
        if(user == null) return null;
        return UserDto.builder()
                .id(user.getId())
                .email(user.getEmail())
                .phone(user.getPhone())
                .role(user.getRole())
                .build();
    }
}
