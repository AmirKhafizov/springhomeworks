package ru.iteco.json.mapper;

import org.springframework.stereotype.Component;
import ru.iteco.json.dto.OrderDto;
import ru.iteco.models.Order;
import ru.iteco.services.DishService;

/**
 * Маппер модели заказа в DTO
 */
@Component
public class OrderMapper implements Mapper<Order, OrderDto> {

    private DishService dishService;

    public OrderMapper(DishService dishService) {
        this.dishService = dishService;
    }

    @Override
    public OrderDto map(Order order) {
        if (order == null) return  null;
        OrderDto orderDto = OrderDto.builder()
                .startTime(order.getStartTime())
                .endTime(order.getEndTime())
                .cost(order.getCost())
                .userDtoId(order.getUser().getId())
                .placeDtoId(order.getPlace().getId())
                .dishIds(dishService.getIds(order.getDishes()))
                .build();
        orderDto.setId(order.getId());
        return orderDto;
    }
}
