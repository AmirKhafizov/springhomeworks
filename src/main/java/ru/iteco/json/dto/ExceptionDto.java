package ru.iteco.json.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO ошибок
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExceptionDto {
    /**
     * Поле тело сообщения ошибки
     */
    private String message;

    /**
     * Поле название системы
     */
    private String system;

    public static ExceptionDto of(String message,String system) {
        return new ExceptionDto(message,system);
    }
}
