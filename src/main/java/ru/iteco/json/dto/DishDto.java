package ru.iteco.json.dto;

import lombok.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.UUID;

/**
 * DTO блюда
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DishDto {
    /**
     * Поле id блюда
     */
    private UUID id;

    /**
     * Поле названия блюда
     */
    private String name;

    /**
     * Поле описания блюда
     */
    private String description;

    /**
     * Поле количества калорий блюда
     */
    private Integer calorieCount;

    /**
     * Поле id заведения
     */
    private UUID placeDtoId;

    /**
     * Поле id категории блюда
     */
    private UUID dishCategoryId;

    /**
     * Поле цены блюда
     */
    private BigDecimal price;

    /**
     * Поле граммовки блюда
     */
    private BigInteger weight;
}
