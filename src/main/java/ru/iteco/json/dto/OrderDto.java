package ru.iteco.json.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import ru.iteco.utils.InstantCustomDeserializer;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * DTO заказа
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {
    /**
     * Поле id заказа
     */
    private UUID id;

    /**
     * Поле даты и времени начала заказа
     */
    @JsonDeserialize(using = InstantCustomDeserializer.class)
    private Instant startTime;

    /**
     * Поле даты и времени окончания заказа
     */
    private Instant endTime;

    /**
     * Поле id пользователя, сделавшего заказ
     */
    private UUID userDtoId;

    /**
     * Поле id заказанных блюд
     */
    private List<UUID> dishIds;

    /**
     * Поле стоимости заказа
     */
    private BigDecimal cost;

    /**
     * Поле id заведения. осущуствляющего заказ
     */
    private UUID placeDtoId;
}
