package ru.iteco.json.dto;

import lombok.*;
import ru.iteco.security.role.Role;

import java.util.List;
import java.util.UUID;

/**
 * DTO пользователя
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private UUID id;
    /**
     * Поле почта пользователя
     */
    private String email;

    /**
     * Поле телефон пользлователя
     */
    private String phone;

    /**
     * Поле роль пользователя
     */
    private Role role;

    /**
     * Поле список ошибок при валидации
     */
    private List<ValidationErrorDto> errors;

    /**
     * Поле пароль пользователя
     */
    private String password;

}
