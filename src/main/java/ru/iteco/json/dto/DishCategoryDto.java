package ru.iteco.json.dto;

import lombok.*;

import java.util.UUID;

/**
 * DTO категории блюда
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DishCategoryDto {
    /**
     * Поле id категории блюда
     */
    private UUID id;

    /**
     * Поле названия категории блюда
     */
    private String name;

    /**
     * Поле возможность удаления категории блюда
     */
    private Boolean canRemove;
}
