package ru.iteco.json.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ValidationErrorDto {
    /**
     * Поле тело сообщения ошибки
     */
    private String message;
}
