package ru.iteco.json.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

import java.time.Instant;
import java.time.LocalTime;
import java.util.UUID;

/**
 * DTO заведения
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PlaceDto {
    /**
     * Поле id заведения
     */
    private UUID id;

    /**
     * Поле название заведения
     */
    private String name;

    /**
     * Поле краткое описание заведения
     */
    private String about;

    /**
     * Поле адресс заведения
     */
    private String address;

    /**
     * Поле телефон заведения
     */
    private String phone;

    /**
     * Поле id администратора заведения
     */
    private UUID userDtoId;

    /**
     * Поле время на доставку
     */
    @JsonFormat(pattern = "HH:mm:ss")
    private LocalTime timeOfDelivery;
}
