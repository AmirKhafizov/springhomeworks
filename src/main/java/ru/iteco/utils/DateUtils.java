package ru.iteco.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class DateUtils {
    /**
     * Метод конвертер миллисекундны в LocalDateTime
     *
     * @param millis милисекудны
     * @return LocalDateTime
     */
    public static LocalDateTime millsToLocalDateTime(Long millis) {
        Instant instant = Instant.ofEpochMilli(millis);
        return instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
