package ru.iteco.exception;

import java.util.UUID;

/**
 * Исключение не найденного объекта
 */
public class NotFoundException extends RuntimeException {

    public NotFoundException() {
    }

    public NotFoundException(String message) {
        super(message);
    }
}
