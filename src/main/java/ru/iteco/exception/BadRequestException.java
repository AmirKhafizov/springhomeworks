package ru.iteco.exception;

import java.util.UUID;

/**
 * Исключение не правильных запросов
 */
public class BadRequestException extends RuntimeException {

    public BadRequestException() {
    }

    public BadRequestException(String message) {
        super(message);
    }

}
