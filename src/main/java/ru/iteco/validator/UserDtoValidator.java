package ru.iteco.validator;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.iteco.application.Patterns;
import ru.iteco.json.dto.UserDto;
import ru.iteco.json.dto.ValidationErrorDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Класс валидации пользователя
 */
@Component
public class UserDtoValidator {

    private static final Logger logger = LogManager.getLogger(UserDtoValidator.class.getName());
    private MessageSource messageSource;

    public UserDtoValidator(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public List<ValidationErrorDto> validate(UserDto dto){
        List<ValidationErrorDto> errors = new ArrayList<>();
        String email = dto.getEmail();
        if(email.isEmpty()){
            logger.error("email is empty");
            String message = messageSource.getMessage("email.empty", new Object[]{}, Locale.getDefault());
            errors.add(new ValidationErrorDto(message));
        } else {
            EmailValidator emailValidator = EmailValidator.getInstance();
            if(!emailValidator.isValid(email)){
                logger.error("bad-email");
                String message = messageSource.getMessage("email.invalid", new Object[]{}, Locale.getDefault());
                errors.add(new ValidationErrorDto(message));
            }
        }
        String phone = dto.getPhone();
        if(phone.isEmpty()){
            logger.error("hone is empty");
            String message = messageSource.getMessage("phone.empty", new Object[]{}, Locale.getDefault());
            errors.add(new ValidationErrorDto(message));
        } else {
            if(!Pattern.matches(Patterns.VALID_PHONE_REGEX, dto.getPhone())){
                logger.error("bad-phone");
                String message = messageSource.getMessage("phone.invalid", new Object[]{}, Locale.getDefault());
                errors.add(new ValidationErrorDto(message));
            }
        }
        return errors;
    }

}
