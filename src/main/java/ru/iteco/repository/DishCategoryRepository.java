package ru.iteco.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.iteco.models.menu.DishCategory;

import java.util.UUID;

@Repository
public interface DishCategoryRepository extends JpaRepository<DishCategory, UUID>, JpaSpecificationExecutor<DishCategory> {
}
