package ru.iteco.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.exception.BadRequestException;
import ru.iteco.exception.NotFoundException;
import ru.iteco.json.dto.DishCategoryDto;
import ru.iteco.json.mapper.DishCategoryMapper;
import ru.iteco.models.menu.DishCategory;
import ru.iteco.repository.DishCategoryRepository;

import java.util.UUID;

/**
 * Сервис категории блюда
 */
@Service
@Transactional
public class DishCategoryServiceImpl implements DishCategoryService {

    private DishCategoryRepository dishCategoryRepository;

    private DishCategoryMapper dishCategoryMapper;

    public DishCategoryServiceImpl(DishCategoryRepository dishCategoryRepository,
                                   DishCategoryMapper dishCategoryMapper) {
        this.dishCategoryRepository = dishCategoryRepository;
        this.dishCategoryMapper = dishCategoryMapper;
    }

    @Override
    public DishCategoryDto createDishCategory(DishCategoryDto dto) {
        DishCategory newDishCategory = DishCategory.builder()
                .name(dto.getName())
                .canRemove(dto.getCanRemove())
                .build();
        dishCategoryRepository.save(newDishCategory);
        return dishCategoryMapper.map(newDishCategory);
    }

    @Override
    public DishCategoryDto updateDishCategory(UUID dishCategoryId, DishCategoryDto dto) {
        DishCategory dishCategory = getDishCategory(dishCategoryId);
        dishCategory.setName(dto.getName());
        dishCategoryRepository.save(dishCategory);
        return dishCategoryMapper.map(dishCategory);
    }

    @Override
    public void deleteDishCategory(UUID dishCategoryId){
        DishCategory dishCategory = getDishCategory(dishCategoryId);
        if(dishCategory.getCanRemove()){
           dishCategoryRepository.deleteById(dishCategoryId);
        } else throw new BadRequestException("Dish category with id=" + dishCategoryId + " can't be deleted");
    }

    @Override
    public DishCategory getDishCategory(UUID dishCategoryId) {
        return dishCategoryRepository.findById(dishCategoryId)
                .orElseThrow(() -> new NotFoundException("Dish category with id=" + dishCategoryId + " not found"));
    }

    @Override
    public DishCategoryDto getDishCategoryDto(UUID dishCategoryId) {
        return dishCategoryMapper.map(getDishCategory(dishCategoryId));
    }
}
