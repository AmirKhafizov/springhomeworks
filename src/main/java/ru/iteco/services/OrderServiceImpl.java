package ru.iteco.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.exception.NotFoundException;
import ru.iteco.json.dto.OrderDto;
import ru.iteco.json.mapper.OrderMapper;
import ru.iteco.models.Order;
import ru.iteco.repository.OrderRepository;

import java.time.Instant;
import java.time.temporal.ChronoField;
import java.util.UUID;

/**
 * Сервис заказа доставки
 */
@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;

    private UserService userService;

    private DishService dishService;

    private OrderMapper orderMapper;

    private PlaceService placeService;

    public OrderServiceImpl(OrderRepository orderRepository,
                            UserService userService,
                            DishService dishService,
                            OrderMapper orderMapper,
                            PlaceService placeService) {
        this.orderRepository = orderRepository;
        this.userService = userService;
        this.dishService = dishService;
        this.orderMapper = orderMapper;
        this.placeService = placeService;
    }

    @Override
    public OrderDto createOrder(OrderDto dto) {
        Order newOrder = Order.builder()
                .startTime(dto.getStartTime())
                .endTime(getEndOrderLocalDateTime(dto.getStartTime(),dto.getPlaceDtoId()))
                .cost(dto.getCost())
                .user(userService.getUser(dto.getUserDtoId()))
                .dishes(dishService.getByIdsIn(dto.getDishIds()))
                .place(placeService.getPlace(dto.getPlaceDtoId()))
                .build();
        orderRepository.save(newOrder);
        return orderMapper.map(newOrder);
    }

    @Override
    public OrderDto updateOrder(UUID orderId, OrderDto dto) {
        Order order = getOrder(orderId);
        order.setStartTime(dto.getStartTime());
        order.setEndTime(getEndOrderLocalDateTime(dto.getStartTime(),dto.getPlaceDtoId()));
        order.setCost(dto.getCost());
        order.setDishes(dishService.getByIdsIn(dto.getDishIds()));
        orderRepository.save(order);
        return orderMapper.map(order);
    }

    @Override
    public void deleteOrder(UUID orderId) {
        Order order = getOrder(orderId);
        orderRepository.delete(order);
    }

    @Override
    public Order getOrder(UUID orderId) {
        return orderRepository.findById(orderId)
                .orElseThrow(() -> new NotFoundException("Order with id=" + orderId + " not found"));
    }

    @Override
    public OrderDto getOrderDto(UUID orderId) {
        return orderMapper.map(getOrder(orderId));
    }

    private Instant getEndOrderLocalDateTime(Instant startOrderTime, UUID placeId){
        return  startOrderTime.plusMillis(placeService.getPlace(placeId).getTimeOfDelivery().get(ChronoField.MILLI_OF_DAY));
    }
}
