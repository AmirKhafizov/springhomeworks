package ru.iteco.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.exception.NotFoundException;
import ru.iteco.json.dto.DishDto;
import ru.iteco.json.mapper.DishMapper;
import ru.iteco.models.menu.Dish;
import ru.iteco.repository.DishRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Сервис блюда
 */
@Service
@Transactional
public class DishServiceImpl implements DishService {

    private DishRepository dishRepository;

    private PlaceService placeService;

    private DishCategoryService dishCategoryService;

    private DishMapper dishMapper;

    public DishServiceImpl(DishRepository dishRepository,
                           PlaceService placeService,
                           DishCategoryService dishCategoryService,
                           DishMapper dishMapper) {
        this.dishRepository = dishRepository;
        this.placeService = placeService;
        this.dishCategoryService = dishCategoryService;
        this.dishMapper = dishMapper;
    }

    @Override
    public DishDto createDish(DishDto dto) {
        Dish newDish = Dish.builder()
                .name(dto.getName())
                .weight(dto.getWeight())
                .price(dto.getPrice())
                .calorieCount(dto.getCalorieCount())
                .description(dto.getDescription())
                .place(placeService.getPlace(dto.getPlaceDtoId()))
                .dishCategory(dishCategoryService.getDishCategory(dto.getDishCategoryId()))
                .build();
        dishRepository.save(newDish);
        return dishMapper.map(newDish);
    }

    @Override
    public DishDto updateDish(UUID dishId, DishDto dto) {
        Dish dish = getDish(dishId);

        dish.setName(dto.getName());
        dish.setCalorieCount(dto.getCalorieCount());
        dish.setDescription(dto.getDescription());
        dish.setDishCategory(dishCategoryService.getDishCategory(dto.getDishCategoryId()));
        dish.setWeight(dto.getWeight());
        dish.setPrice(dto.getPrice());

        dishRepository.save(dish);
        return dishMapper.map(dish);
    }

    @Override
    public void deleteDish(UUID dishId) {
        dishRepository.deleteById(dishId);
    }

    @Override
    public Dish getDish(UUID dishId) {
        return dishRepository.findById(dishId)
                .orElseThrow(() -> new NotFoundException("Dish with id=" + dishId + " not found"));
    }

    @Override
    public DishDto getDishDto(UUID dishId) {
        return dishMapper.map(getDish(dishId));
    }

    @Override
    public List<Dish> getByIdsIn(List<UUID> ids) {
        return dishRepository.findAllByIdIn(ids);
    }

    @Override
    public List<UUID> getIds(List<Dish> dishes) {
        List<UUID> uuids = new ArrayList<>();
        for(Dish dish: dishes){
            uuids.add(dish.getId());
        }
        return uuids;
    }
}
