package ru.iteco.services;

import ru.iteco.json.dto.PlaceDto;
import ru.iteco.models.Place;

import java.util.UUID;

/**
 * Интерфейс сервиса заведения
 */
public interface PlaceService{
    /**
     * Метод создания заведения
     *
     * @param dto создания заведения
     * @return DTO заведения
     */
        PlaceDto createPlace(PlaceDto dto);

    /**
     * Метод обновления информации о заведении
     *
     * @param placeId id заведения
     * @param dto обновления информации о заведении
     * @return DTO заведения
     */
    PlaceDto updatePlace(UUID placeId, PlaceDto dto);

    /**
     * Метод удаления заведения
     *
     * @param placeId id заведения
     */
    void deletePlace(UUID placeId);

    /**
     * Метод получения модели заведения
     *
     * @param placeId id заведения
     * @return модель заведения
     */
    Place getPlace(UUID placeId);

    /**
     * Метод получения DTO заведения
     *
     * @param placeId id заведения
     * @return DTO заведения
     */
    PlaceDto getPlaceDto(UUID placeId);
}
