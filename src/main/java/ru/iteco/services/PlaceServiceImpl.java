package ru.iteco.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.exception.NotFoundException;
import ru.iteco.json.dto.PlaceDto;
import ru.iteco.json.mapper.PlaceMapper;
import ru.iteco.models.Place;
import ru.iteco.repository.PlaceRepository;

import java.util.UUID;

/**
 * Сервис заведения
 */
@Service
@Transactional
public class PlaceServiceImpl implements PlaceService {

    private PlaceRepository placeRepository;

    private UserService userService;

    private PlaceMapper placeMapper;

    public PlaceServiceImpl(PlaceRepository placeRepository,
                            UserService userService,
                            PlaceMapper placeMapper
                            ) {
        this.placeRepository = placeRepository;
        this.userService = userService;
        this.placeMapper = placeMapper;
    }

    @Override
    public PlaceDto createPlace(PlaceDto dto) {
        Place newPlace = Place.builder()
                .name(dto.getName())
                .about(dto.getAbout())
                .phone(dto.getPhone())
                .address(dto.getAddress())
                .user(userService.getUser(dto.getUserDtoId()))
                .timeOfDelivery(dto.getTimeOfDelivery())
                .build();
        placeRepository.save(newPlace);
        return placeMapper.map(newPlace);
    }

    @Override
    public PlaceDto updatePlace(UUID placeId, PlaceDto dto) {
        Place place = getPlace(placeId);
        place.setName(dto.getName());
        place.setAbout(dto.getAbout());
        place.setPhone(dto.getPhone());
        place.setAddress(dto.getAddress());
        place.setTimeOfDelivery(dto.getTimeOfDelivery());
        placeRepository.save(place);
        return placeMapper.map(place);
    }

    @Override
    public void deletePlace(UUID placeId) {
        placeRepository.deleteById(placeId);
    }

    @Override
    public Place getPlace(UUID placeId) {
        return placeRepository.findById(placeId)
                .orElseThrow(() -> new NotFoundException("Place with id=" + placeId + " not found"));
    }

    @Override
    public PlaceDto getPlaceDto(UUID placeId) {
        return placeMapper.map(getPlace(placeId));
    }
}
