package ru.iteco.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.exception.NotFoundException;
import ru.iteco.json.dto.UserDto;
import ru.iteco.json.dto.ValidationErrorDto;
import ru.iteco.json.mapper.UserMapper;
import ru.iteco.models.User;
import ru.iteco.repository.UserRepository;
import ru.iteco.validator.UserDtoValidator;

import java.util.List;
import java.util.UUID;

/**
 * Сервис пользователя
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private UserMapper userMapper;

    private UserDtoValidator userDtoValidator;

    public UserServiceImpl(UserRepository userRepository,
                           UserMapper userMapper,
                           UserDtoValidator userDtoValidator) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.userDtoValidator = userDtoValidator;
    }

    @Override
    public User getUser(UUID userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("User with id=" + userId + " not found"));
    }

    @Override
    public UserDto getUserDto(UUID userId) {
        return userMapper.map(getUser(userId));
    }

    @Override
    public UserDto register(UserDto dto) {
        List<ValidationErrorDto> errors = userDtoValidator.validate(dto);
        if(!errors.isEmpty()){
            return UserDto.builder()
                    .errors(errors)
                    .build();
        }
        User newUser = User.builder()
                .email(dto.getEmail())
                .password(dto.getPassword())
                .phone(dto.getPhone())
                .role(dto.getRole())
                .build();
        userRepository.save(newUser);
        return userMapper.map(newUser);
    }
}
