package ru.iteco.services;

import ru.iteco.json.dto.UserDto;
import ru.iteco.models.User;

import java.util.UUID;

/**
 * Интерфейс сервиса пользователя
 */
public interface UserService{
    /**
     * Метод получения модели пользователя
     *
     * @param userId id пользователя
     * @return модель пользователя
     */
    User getUser(UUID userId);

    /**
     * Метод получения DTO пользователя
     *
     * @param userId id пользователя
     * @return DTO пользователя
     */
    UserDto getUserDto(UUID userId);

    /**
     * Метод получения DTO пользователя
     *
     * @param dto регистрации пользователя
     * @return DTO пользователя
     */
    UserDto register(UserDto dto);
}
