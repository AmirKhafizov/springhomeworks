package ru.iteco.services;

import ru.iteco.json.dto.DishDto;
import ru.iteco.models.menu.Dish;

import java.util.List;
import java.util.UUID;

/**
 * Интерфейс сервиса блюда
 */
public interface DishService{
    /**
     * Метод создания блюда
     *
     * @param dto создания блюда
     * @return DTO блюда
     */
    DishDto createDish(DishDto dto);

    /**
     * Метод обновления информации о блюде
     *
     * @param dto обновления блюда
     * @return DTO блюда
     */
    DishDto updateDish(UUID dishId, DishDto dto);

    /**
     * Метод удаления блюда
     *
     * @param dishId id блюда
     */
    void deleteDish(UUID dishId);

    /**
     * Метод получения модели блюда
     *
     * @param dishId id блюда
     * @return модель блюда
     */
    Dish getDish(UUID dishId);

    /**
     * Метод получения DTO блюда
     *
     * @param dishId id блюда
     * @return DTO блюда
     */
    DishDto getDishDto(UUID dishId);

    /**
     * Метод получения блюд по их id
     *
     * @param ids id блюд
     * @return список блюд
     */
    List<Dish> getByIdsIn(List<UUID> ids);

    /**
     * Метод получения id блюд
     *
     * @param dishes блюда
     * @return список id блюд
     */
    List<UUID> getIds(List<Dish> dishes);
}
