package ru.iteco.services;

import ru.iteco.json.dto.OrderDto;
import ru.iteco.models.Order;

import java.util.UUID;


/**
 * Интерфейс сервиса заказа доставки
 */
public interface OrderService{
    /**
     * Метод создания заказа
     *
     * @param dto создания заказа
     * @return DTO заказа
     */
    OrderDto createOrder(OrderDto dto);

    /**
     * Метод обновления информации о заказе
     *
     * @param orderId id заказа
     * @param dto обновления заказа
     * @return DTO заказа
     */
    OrderDto updateOrder(UUID orderId, OrderDto dto);

    /**
     * Метод удаления заказа
     *
     * @param orderId id заказа
     */
    void deleteOrder(UUID orderId);

    /**
     * Метод получения модели заказа
     *
     * @param orderId id заказа
     * @return модель заказа
     */
    Order getOrder(UUID orderId);

    /**
     * Метод получения DTO заказа
     *
     * @param orderId id заказа
     * @return DTO заказа
     */
    OrderDto getOrderDto(UUID orderId);
}
