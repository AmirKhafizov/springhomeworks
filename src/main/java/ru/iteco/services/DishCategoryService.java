package ru.iteco.services;

import ru.iteco.json.dto.DishCategoryDto;
import ru.iteco.models.menu.DishCategory;

import java.util.UUID;

/**
 * Интерфейс сервиса категории блюда
 */
public interface DishCategoryService{
    /**
     * Метод создания категории блюда
     *
     * @param dto форма добавления блюда
     * @return DTO категории блюда
     */
    DishCategoryDto createDishCategory(DishCategoryDto dto);

    /**
     * Метод обновления блюда
     *
     * @param dishCategoryId id катгории блюда
     * @param dto форма обновления блюда
     * @return DTO категории блюда
     */
    DishCategoryDto updateDishCategory(UUID dishCategoryId, DishCategoryDto dto);

    /**
     * Метод удаления блюда
     *
     * @param dishCategoryId id категории блюда
     */
    void deleteDishCategory(UUID dishCategoryId);

    /**
     * Метод получения модели категории блюда
     *
     * @param dishCategoryId id категории блюда
     * @return модель категории блюда
     */
    DishCategory getDishCategory(UUID dishCategoryId);

    /**
     * Метод получения DTO категории блюда
     *
     * @param dishCategoryId id категории блюда
     * @return DTO категории блюда
     */
    DishCategoryDto getDishCategoryDto(UUID dishCategoryId);
}
