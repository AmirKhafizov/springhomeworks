package ru.iteco.application;

/**
 * Интерфейс регулярных выражений
 */
public interface Patterns {
    /**
     * Регульрное выражение для проверки валидации номера телефона
     */
    String VALID_PHONE_REGEX = "(\\+?[0-9]{11})?";
}
